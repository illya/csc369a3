#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "ext2.h"
#include <assert.h>
#include <errno.h>
#include <string.h>

#define BASE_OFFSET 1024  /* location of the super-block in the first group */
#define BLOCK_OFFSET(block) (BASE_OFFSET + (block - 1) * EXT2_BLOCK_SIZE)
#define MIN_DE_SIZE 8
unsigned char *disk;
unsigned int a_flag;
char *path_src;
char *path_targ;

//Given a directory entry, returns a null terminated version of its name
char *de_name_to_str(struct ext2_dir_entry_2 *de) {
    
    char *filename = (char *)malloc(sizeof(char)*(de->name_len + 1));

    if(filename == NULL) {
        perror("malloc");
        exit(1);
    }
    memcpy(filename, de->name, de->name_len);

    filename[de->name_len] = '\0';

    return filename;
}

//Finds a free block and returns the block number
int get_free_block(){

    struct ext2_group_desc *gd = (struct ext2_group_desc *) (disk +2*1024);
    unsigned int *bmap = (unsigned int *)
        (disk + gd->bg_block_bitmap * EXT2_BLOCK_SIZE);
    int i;
    int j;

    
    if(gd->bg_free_blocks_count >= 128) 
    {
        fprintf(stderr, "Out of blocks\n");
        return ENOSPC;
    }
    for(i = 0; i < 4; i++)
    {
        unsigned int bit = *(bmap + i);
        for(j = 0; j < 32; j++, bit >>= 1)
        {
            if((bit & 1) == 0)
            {
                *(bmap + i) |= (1 << j); 
                gd->bg_free_blocks_count--;   
                return i * 32 + j + 1;
            }
        }
    }
    return -1;
}

//Given a parent inode, a entry name and link inode number,
//writes the new directory entry with the appropriate information
//in the parent inode's blocks if space is available.
unsigned int write_de_to_block(struct ext2_inode *parent_inode, 
    char* new_de_name, int link_inum){
    
    int * pointers = (int *) parent_inode->i_block;
    int last_used_block_index, next_free_block_index, i;
    int next_free_block = -1;
    int last_used_block = -1;
    struct ext2_dir_entry_2 *de;
    int no_next_free_block = 1;
    int indirection = 0;

    unsigned int *singly_indirect_bp = (unsigned int *)
        (disk + BLOCK_OFFSET(parent_inode->i_block[12]));

    //Find the last used block
    for(i=0;i<15;i++){
       
        if(pointers[i]!=0){
            last_used_block_index = i;
            next_free_block_index = i+1;
        } 
    }
    //handle single indirection if needed
    if(last_used_block_index == 12){
        indirection =1;
        
        unsigned int direct_bp = 0;
        while(direct_bp < (EXT2_BLOCK_SIZE / sizeof(unsigned int))
              && singly_indirect_bp[direct_bp] > 0) {

            last_used_block = singly_indirect_bp[direct_bp];

            //If there is any space left, assign the next free block index
            if(direct_bp != (EXT2_BLOCK_SIZE / sizeof(unsigned int))-1){
                next_free_block_index = direct_bp+1; 
                no_next_free_block = 0;
            }
            else{
                no_next_free_block=1;
            }
            direct_bp++;
        }        
    }
    else if(next_free_block_index == 12){
        //Find next_free_block in the indirect table
        indirection = 1;

        unsigned int direct_bp = 0;
        while(direct_bp < (EXT2_BLOCK_SIZE / sizeof(unsigned int))
              && singly_indirect_bp[direct_bp] > 0) {

            next_free_block_index = direct_bp ; 
            no_next_free_block = 0;
            direct_bp++;
        }
    }
    else {
        //No indirection -> Last used block is simply pointers[index]
        last_used_block = pointers[last_used_block_index];
        no_next_free_block = 0;
    }


    de = (struct ext2_dir_entry_2 *)
                    (disk + BLOCK_OFFSET(last_used_block));

    unsigned int size = 0;
    unsigned int  last_de_size = 0;
    unsigned int leftover_size = 0;
    unsigned int necessary_size = 0;

    //Loop through the directory entries of the last used block
    while (size < EXT2_BLOCK_SIZE) {
        //char *filename = de_name_to_str(de);
        //printf("looking at %s with size %d\n", filename, de->rec_len);  
        //free(filename);
        
        size += de->rec_len;

        //Necessary for some reason
        if(size >= EXT2_BLOCK_SIZE){
           break; 
        }

        de = (void *)de + de->rec_len;
        last_de_size = de->rec_len;          
    }

    //Calculate space necessary for our new entry and how much space is le ft
    int last_de_name_size =  de->name_len;
    int new_de_name_size =  strlen(new_de_name);
    while(last_de_name_size % 4 !=0){
        last_de_name_size++;
    }
    while(new_de_name_size % 4 !=0){
        new_de_name_size++;
    }  
    leftover_size = last_de_size - (MIN_DE_SIZE + last_de_name_size);
    necessary_size = MIN_DE_SIZE + new_de_name_size;

    if(leftover_size > necessary_size){
        //We have space in the last used block for our new entry

        de->rec_len = (MIN_DE_SIZE + last_de_name_size);
        //Give our new de all the appropriate information
        de = (void *)de + de->rec_len;
        de->name_len = strlen(new_de_name);
        de->rec_len = leftover_size;
        strcpy(de->name, new_de_name);
        de->inode = link_inum;
        de->file_type = EXT2_FT_SYMLINK;

        return  BLOCK_OFFSET(last_used_block)+EXT2_BLOCK_SIZE-leftover_size-8;
    }
    else{
        //no memory left in last used block
        if(no_next_free_block){
            printf("Our of blocks\n");
            return ENOSPC;
        }
        else{
            if(indirection ==1){
                next_free_block = get_free_block();
                singly_indirect_bp[next_free_block_index] = next_free_block;
            }
            else{
                next_free_block = get_free_block();
                pointers[last_used_block_index+1] = next_free_block;
            }
  
            //Place de in new block (next free block)
            de = (void*)(disk + BLOCK_OFFSET(next_free_block));
            de->name_len = strlen(new_de_name);
            de->rec_len = EXT2_BLOCK_SIZE;
            strcpy(de->name, new_de_name);
            de->inode = link_inum;
            de->file_type = EXT2_FT_SYMLINK;
            parent_inode->i_blocks++;

        }
        return BLOCK_OFFSET(next_free_block);
    }
    return 0;
}

//Finds a free inode number
int find_inode(struct ext2_super_block *sb, struct ext2_group_desc *gd) {
    
    unsigned char *inode_bitmap_bp = (unsigned char *)
        (disk + BLOCK_OFFSET(gd->bg_inode_bitmap));
    unsigned int free_inode = 1;
    int i;

    if(sb->s_free_inodes_count == 0) {
        return -1;
    } 
    for ( i = 0;
         i < (sb->s_inodes_count / (sizeof(unsigned char) * 8)); i++) {
        int j;
        for( j = 0; j < 8; j++) {
            if(!((inode_bitmap_bp[i] >> j) & 0x01)) {
                inode_bitmap_bp[i] ^= (-1 ^ inode_bitmap_bp[i]) & (1 << j);
                sb->s_free_inodes_count--;
                gd->bg_free_inodes_count--;   
                return free_inode;
            }
            free_inode++;
        }
    }
    return -1;
}


//Given a string s, counts how many instances of '/' appear
int countDirs( char* s){
    return *s == '\0'
              ? 0
              : countDirs( s + 1) + (*s == '/');
}

int main(int argc, char **argv) {
   
    //Checking syntax
    if(argc == 4) {
        path_src = strdup(argv[2]);
        path_targ = strdup(argv[3]);
        if(argv[2][0]!='/' || argv[3][0]!='/' ){
            fprintf(stderr, "Usage: ext2_ls <image file name>"
                "  option -a <SRC> <TARG>\n");
            exit(1);   
        }
    }
    if(argc == 5){
        path_src = strdup(argv[3]);
        path_targ = strdup(argv[4]);
        if(argv[3][0]!='/' || argv[4][0]!='/' || strcmp(argv[2], "-s")!=0){

            printf("Arg3 = %s, Arg4 = %s, arg2 = %s\n", 
                argv[3], argv[4], argv[2]);
            fprintf(stderr, "Usage: ext2_ls <image file name>"
            "  option -s <SRC> <TARG>\n");
            exit(1);   
        } 
    }

    //Opening virtual image
    int fd = open(argv[1], O_RDWR);
    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
        perror("mmap");
        exit(1);
    }
    //Path must start with a backslash
    if (path_src[0] != '/' || path_targ[0]!= '/') {   
        fprintf(stderr, "Could not find directory/file\n");
        return ENOENT;
    }

    
    struct ext2_super_block *sb = (struct ext2_super_block *)(disk + 1024);
    struct ext2_group_desc *gd = (struct ext2_group_desc *)(disk + 2048);
    unsigned int file_inum = EXT2_ROOT_INO;
    unsigned int src_inum;
    struct ext2_inode *file_inode = (struct ext2_inode *)
    (disk + (BLOCK_OFFSET(gd->bg_inode_table) + (file_inum - 1) *
             sizeof(struct ext2_inode)));
    struct ext2_dir_entry_2 *path_de;
    
    // If argc == 4 and we passed the syntax check, 
    // we need to find the source file 
    if(argc==4){

        char *path_src_tokens = strtok(path_src, "/");
        if(path_src_tokens == NULL) {
            printf("Source refers to a directory\n");
            return EISDIR;   
        }

        //Loop through directory names
        while(path_src_tokens != NULL) {
            
            unsigned int found = 0;
            unsigned int index = 0;

            //Loop through block indices until we find our directory
            while (!found && index < 15 && file_inode->i_block[index] > 0) {

                //Handle single indirection
                if (index == 12) {

                    unsigned int *singly_indirect_bp = (unsigned int *)
                    (disk + BLOCK_OFFSET(file_inode->i_block[index]));
                    
                    unsigned int direct_bp = 0;

                    //Loop through directory entries
                    while(!found && direct_bp <
                          (EXT2_BLOCK_SIZE / sizeof(unsigned int))
                          && singly_indirect_bp[direct_bp] > 0) {
                        
                        path_de = (struct ext2_dir_entry_2 *)
                        (disk + BLOCK_OFFSET(singly_indirect_bp[direct_bp]));
                        
                        unsigned int size = 0;
                        while (size < EXT2_BLOCK_SIZE && path_de->inode) {    
                            char *filename = de_name_to_str(path_de);

                            //If we found it, save out necessary info and break
                            if (!(strcmp(path_src_tokens, filename))) {
                                found = 1;
                                src_inum = path_de->inode;
                                free(filename);
                                break;
                            }

                            free(filename);
                            size += path_de->rec_len;
                            path_de = (void *)path_de + path_de->rec_len;
                        }
                        direct_bp++;
                    }
                } else {     
                    //No single indirection
                    path_de = (struct ext2_dir_entry_2 *)
                        (disk + BLOCK_OFFSET(file_inode->i_block[index]));
                    unsigned int size = 0;

                    //loop through entries
                    while (size < EXT2_BLOCK_SIZE && path_de->inode) {
                        

                        char *filename = de_name_to_str(path_de);
                        //printf("looking at %s\n", filename);

                        //If we found it, save out necessary info and break
                        if (!(strcmp(path_src_tokens, filename))) {

                            //printf("Found it! \n");
                            found = 1;
                            src_inum = path_de->inode;
                            free(filename);
                            break;
                        }
                        free(filename);
                        size += path_de->rec_len;
                        path_de = (void *)path_de + path_de->rec_len;
                    }
                }               
                index++;
            }
            //If we looped through every entry and didnt find it, return ENOENT
            if (!found) {
                fprintf(stderr, "Could not find source file\n");
                return ENOENT;
            }

            path_src_tokens = strtok (NULL, "/");


            if (path_de->file_type != 2 && found) {
                //Found a non directory at the end
                file_inum = path_de->inode;
                file_inode = (struct ext2_inode *)
                (disk + (BLOCK_OFFSET(gd->bg_inode_table) + (file_inum - 1) *
                         sizeof(struct ext2_inode)));
            } else  if (path_de->file_type == 2) {
                //Found a directory
                file_inum = path_de->inode;
                file_inode = (struct ext2_inode *)
                (disk + (BLOCK_OFFSET(gd->bg_inode_table) + (file_inum - 1) *
                         sizeof(struct ext2_inode)));   
            } else  if (path_src == NULL) {
                printf("Source refers to a directory\n");
                return EISDIR;  
            }
        }
    }

    //Find targ parent
    int found_count=0;
    int total_targ_dirs; 
    file_inum = EXT2_ROOT_INO;
    file_inode = (struct ext2_inode *)
            (disk + (BLOCK_OFFSET(gd->bg_inode_table) + (file_inum - 1) *
            sizeof(struct ext2_inode)));
    struct ext2_dir_entry_2 *path_de_targ;  
    unsigned int targ_parent_inum;
    struct ext2_inode *targ_parent_inode;
    int dirCount = countDirs(path_targ);

    //Set the total target dir count to its appropriate value
    if(path_targ[strlen(path_targ)-1]=='/')
        total_targ_dirs = dirCount-1;
    else
        total_targ_dirs = dirCount;

    char *path_targ_tokens = strtok(path_targ, "/");

    //if target is root return EISDIR
    if(path_targ_tokens == NULL) {
        printf("Target refers to a directory\n");
        return EISDIR;   
    }

    
    //Loop through directories
    while(path_targ_tokens != NULL) { 
        int found_targ = 0;
        int index_targ = 0;

        //Loop through blocks
        while (!found_targ && index_targ < 15 
            && file_inode->i_block[index_targ] > 0) {
            
            //Handle indirection
            if (index_targ == 12) {
                unsigned int *singly_indirect_bp = (unsigned int *)
                (disk + BLOCK_OFFSET(file_inode->i_block[index_targ]));
                
                unsigned int direct_bp = 0;
                while(!found_targ && direct_bp <
                      (EXT2_BLOCK_SIZE / sizeof(unsigned int))
                      && singly_indirect_bp[direct_bp] > 0) {
                    
                    path_de_targ = (struct ext2_dir_entry_2 *)
                    (disk + BLOCK_OFFSET(singly_indirect_bp[direct_bp]));
                    
                    unsigned int size_targ = 0;
                    while (size_targ < EXT2_BLOCK_SIZE 
                        && path_de_targ->inode) {
                        
                        char *filename = de_name_to_str(path_de_targ);
                        if (!(strcmp(path_targ_tokens, filename))) {
                            found_targ = 1;
                            found_count++;
                            targ_parent_inum = path_de_targ->inode;

                            free(filename);
                            break;
                        }
                        free(filename);
                        size_targ += path_de_targ->rec_len;
                        path_de_targ = (void *)path_de_targ 
                        + path_de_targ->rec_len;
                    }
                    direct_bp++;
                }
            } else {
                //Non-indirection
                path_de_targ = (struct ext2_dir_entry_2 *)
                (disk + BLOCK_OFFSET(file_inode->i_block[index_targ]));
                
                unsigned int size_targ = 0;
                while (size_targ < EXT2_BLOCK_SIZE && path_de_targ->inode) {
                    
                    char *filename = de_name_to_str(path_de_targ);
                    if (!(strcmp(path_targ_tokens, filename))) {
                        targ_parent_inum = path_de_targ->inode;

                        found_targ = 1;
                        found_count++;

                        free(filename);
                        if(found_count == total_targ_dirs){
                            if(path_de_targ->file_type == 2){
                                printf("Target refers to a directory\n");
                                return EISDIR;   
                            }
                            else{
                               printf("Link name already exists\n"); 
                               return EEXIST;
                            }
                            return 0;
                        }
                        break;
                    }
                    free(filename);
                    size_targ += path_de_targ->rec_len;
                    path_de_targ = (void *)path_de_targ 
                    + path_de_targ->rec_len;
                }

            }
            index_targ++;
        }

        if (!found_targ) {
            //Didn't find directory but we are at the last name 
            //Which is the link name
            if(found_count == total_targ_dirs-1){ 
                break;   
            } 
            fprintf(stderr, "Source file does not exist\n");
            return ENOENT;
        }

        path_targ_tokens = strtok (NULL, "/");

        //Found the directory
        if (path_de_targ->file_type == 2) {

            file_inum = path_de_targ->inode;
            file_inode = (struct ext2_inode *)
            (disk + (BLOCK_OFFSET(gd->bg_inode_table) + (file_inum - 1) *
                     sizeof(struct ext2_inode)));
        } else {  
            fprintf(stderr, "Not a directory\n");
            return ENOENT;
        }


    //Increment links count 
    struct ext2_inode * src_inode = (struct ext2_inode *)
            (disk + (BLOCK_OFFSET(gd->bg_inode_table) + (src_inum - 1) *
                     sizeof(struct ext2_inode)));
    src_inode->i_links_count++;
    }
      
    //If total dirs is 1, the link targets parent must be root
    if(total_targ_dirs==1){
        targ_parent_inum = EXT2_ROOT_INO;
    }

    //Initialize target parent inode
    targ_parent_inode = (struct ext2_inode *)
            (disk + (BLOCK_OFFSET(gd->bg_inode_table) 
                + (targ_parent_inum - 1) *
                     sizeof(struct ext2_inode)));
   
    //If we are making a symbolic link, need to create inode
    if(argc == 5){

        //Find free inode
        src_inum = find_inode(sb, gd);

        if(src_inum==-1){
            fprintf(stderr, "Out of inodes\n");
            return ENOSPC;
        }

        //Create new inode at the appropriate number 
        struct ext2_inode * new_inode = (struct ext2_inode *)
            (disk + (BLOCK_OFFSET(gd->bg_inode_table) + (src_inum - 1) *
                     sizeof(struct ext2_inode)));

        //Appropriate meta-data
        new_inode->i_mode = EXT2_S_IFDIR;
        new_inode->i_size = 1024;
        new_inode->i_links_count = 1;
        
        //If we can, fit path into i_block memory
        if(strlen(argv[3])<60){
            memcpy(new_inode->i_block, strdup(argv[3]), strlen(argv[3])+1);
        }
        else{
            //Otherwise we fit the path into the first i_block
            new_inode->i_block[0] = get_free_block();
            char * symlink_path = (char*)(disk + 
                BLOCK_OFFSET(new_inode->i_block[0]));
            memcpy(symlink_path, strdup(argv[3]), strlen(argv[3])+1);
        }
        
    }

    //Write the directory entry at the appropriate spot
    write_de_to_block(targ_parent_inode, path_targ_tokens, src_inum);


    return 0;
}