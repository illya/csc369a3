#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "ext2.h"
#include <errno.h>
#include <string.h>

#define BASE_OFFSET 1024  /* location of the super-block in the first group */
#define BLOCK_OFFSET(block) (BASE_OFFSET + (block - 1) * EXT2_BLOCK_SIZE)

unsigned char *disk;
char *pathname;
char last_path[EXT2_NAME_LEN + 1];
struct ext2_super_block *sb;
struct ext2_group_desc *gd;


char *de_name_to_str(struct ext2_dir_entry_2 *dir_entry) {
    
    
    char *filename = (char *) malloc(sizeof(char) * (dir_entry->name_len + 1));
    memcpy(filename, dir_entry->name, dir_entry->name_len);
    filename[dir_entry->name_len] = '\0';
    return filename;
}


unsigned int allocate_free_inode(struct ext2_super_block *sb,
                             struct ext2_group_desc *gd) {
    
    unsigned char *inode_bitmap_bp = (unsigned char *)
    (disk + BLOCK_OFFSET(gd->bg_inode_bitmap));
    
    unsigned int free_inode = 1;
    for (int i = 0;
         i < (sb->s_inodes_count / (sizeof(unsigned char) * 8)); i++) {
        
        for(int j = 0; j < 8; j++) {
            
            if(!((inode_bitmap_bp[i] >> j) & 0x01)) {
                
                inode_bitmap_bp[i] ^= (-1 ^ inode_bitmap_bp[i]) & (1 << j);
                sb->s_free_inodes_count--;
                gd->bg_free_inodes_count--;
                return free_inode;
            }
            free_inode++;
        }
    }
    return 0;
}


unsigned int allocate_free_block(struct ext2_super_block *sb,
                        struct ext2_group_desc *gd) {
    
    unsigned char *block_bitmap_bp = (unsigned char *)
    (disk + BLOCK_OFFSET(gd->bg_block_bitmap));
    
    unsigned int free_block = 1;
    for (int i = 0;
         i < (sb->s_blocks_count / (sizeof(unsigned char) * 8)); i++) {
        
        for(int j = 0; j < 8; j++) {
            
            if(!((block_bitmap_bp[i] >> j) & 0x01)) {
                
                block_bitmap_bp[i] ^= (-1 ^ block_bitmap_bp[i]) & (1 << j);
                sb->s_free_blocks_count--;
                gd->bg_free_blocks_count--;
                return free_block;
            }
            free_block++;
        }
    }
    return 0;
}


void new_dir_block(struct ext2_inode *dir_inode, unsigned int block) {
    
    block = allocate_free_block(sb, gd);
    struct ext2_dir_entry_2 *dir_entry = (struct ext2_dir_entry_2 *)
    (disk + BLOCK_OFFSET(block));
    
    dir_entry->inode = allocate_free_inode(sb, gd);
    dir_entry->rec_len = EXT2_BLOCK_SIZE;
    dir_entry->name_len = strlen(last_path);
    dir_entry->file_type = EXT2_FT_DIR;
    memcpy(dir_entry->name, last_path, dir_entry->name_len);
}


int main(int argc, char **argv) {
    
    if (argc != 3) {
        
        fprintf(stderr, "Usage: %s <image file name> <absolute path>\n",
                argv[0]);
        exit(1);
    }
    pathname = argv[2];
    
    int fd = open(argv[1], O_RDWR);
    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
        perror("mmap");
        exit(1);
    }
    
    sb = (struct ext2_super_block *)(disk + 1024);
    gd = (struct ext2_group_desc *)(disk + 2048);
    
    if (pathname[0] != '/') {
        
        return ENOENT;
    }
    
    unsigned int dir_inum = EXT2_ROOT_INO;
    struct ext2_inode *dir_inode = (struct ext2_inode *)
    (disk + (BLOCK_OFFSET(gd->bg_inode_table) + (dir_inum - 1) *
             sizeof(struct ext2_inode)));
    struct ext2_dir_entry_2 *dir_entry;
    char *pathname_tokens = strtok(pathname, "/");
    
    if(pathname_tokens == NULL) {
        
        fprintf(stderr, "Is a directory\n");
        return EEXIST;
    }
    
    while (pathname_tokens != NULL) {
        
        if (strlen(pathname_tokens) > EXT2_NAME_LEN) {
            
            fprintf(stderr, "File name too long\n");
            return ENAMETOOLONG;
        }
        
        unsigned int found = 0;
        unsigned int index = 0;
        unsigned int size = 0;
        while (!found && index < 15 && dir_inode->i_block[index] > 0) {
            
            if (index == 12) {
                
                unsigned int *singly_indirect_bp = (unsigned int *)
                (disk + BLOCK_OFFSET(dir_inode->i_block[index]));
                
                unsigned int direct_bp = 0;
                while(!found && direct_bp <
                      (EXT2_BLOCK_SIZE / sizeof(unsigned int))
                      && singly_indirect_bp[direct_bp] > 0) {
                    
                    dir_entry = (struct ext2_dir_entry_2 *)
                    (disk + BLOCK_OFFSET(singly_indirect_bp[direct_bp]));
                    
                    size = 0;
                    while (size < EXT2_BLOCK_SIZE && dir_entry->inode) {
                        
                        char *filename = de_name_to_str(dir_entry);
                        if (!(strcmp(pathname_tokens, filename))) {
                            
                            found = 1;
                            free(filename);
                            break;
                        }
                        free(filename);
                        if (size + dir_entry->rec_len >= EXT2_BLOCK_SIZE) {
                            
                            break;
                        }
                        size += dir_entry->rec_len;
                        dir_entry = (void *)dir_entry + dir_entry->rec_len;
                    }
                    direct_bp++;
                }
            } else {
                
                dir_entry = (struct ext2_dir_entry_2 *)
                (disk + BLOCK_OFFSET(dir_inode->i_block[index]));
                
                size = 0;
                while (size < EXT2_BLOCK_SIZE && dir_entry->inode) {
                    
                    char *filename = de_name_to_str(dir_entry);
                    if (!(strcmp(pathname_tokens, filename))) {
                        
                        found = 1;
                        free(filename);
                        break;
                    }
                    free(filename);
                    if (size + dir_entry->rec_len >= EXT2_BLOCK_SIZE) {
                        
                        break;
                    }
                    size += dir_entry->rec_len;
                    dir_entry = (void *) dir_entry + dir_entry->rec_len;
                }
            }
            index ++;
        }
        snprintf(last_path, EXT2_NAME_LEN + 1, "%s", pathname_tokens);
        pathname_tokens = strtok (NULL, "/");
        if (!found) {
            
            if (pathname_tokens == NULL) {
                
                if(sb->s_free_inodes_count == 0) {
                    
                    return ENOSPC;
                }
                
                if (dir_inode->i_block[0] == 0) {
                    
                    if(sb->s_free_blocks_count == 0) {
                        
                        return ENOSPC;
                        
                    }
                    new_dir_block(dir_inode, dir_inode->i_block[0]);

                } else {
                   
                    dir_entry->rec_len = sizeof(struct ext2_dir_entry_2) +
                    dir_entry->name_len;
                    while((dir_entry->rec_len % 4) != 0) {
                        
                        dir_entry->rec_len++;
                    }
                    
                    if (EXT2_BLOCK_SIZE - size - dir_entry->rec_len <
                        sizeof(struct ext2_dir_entry_2) + strlen(last_path)) {
                        
                        if(sb->s_free_blocks_count == 0) {
                            
                            return ENOSPC;
                        } else if (index == 12) {
                            
                            if (sb->s_free_blocks_count == 1) {
                                
                                return ENOSPC;
                            }
                            
                            dir_inode->i_block[index] = allocate_free_block(sb, gd);
                            unsigned int *singly_indirect_bp = (unsigned int *)
                            (disk + BLOCK_OFFSET(dir_inode->i_block[index]));
                            new_dir_block(dir_inode, singly_indirect_bp[0]);
                            
                        } else if (index == 13) {
                            
                            index--;
                            unsigned int *singly_indirect_bp = (unsigned int *)
                            (disk + BLOCK_OFFSET(dir_inode->i_block[index]));
                            
                            unsigned int direct_bp = 0;
                            while (direct_bp <
                                   (EXT2_BLOCK_SIZE / sizeof(unsigned int))) {
                                
                                if (singly_indirect_bp[direct_bp] == 0) {
                                    
                                    if (sb->s_free_blocks_count == 1) {
                                        
                                        return ENOSPC;
                                    }
                                    
                                    dir_inode->i_block[index] = allocate_free_block(sb, gd);
                                    new_dir_block(dir_inode, singly_indirect_bp[direct_bp]);
                                }
                                direct_bp++;
                            }
                            return ENOSPC;
                        
                        } else {
                            
                            new_dir_block(dir_inode, dir_inode->i_block[index]);
                        }
                        
                        
                    } else {
                        
                        struct ext2_dir_entry_2 *new_dir_entry;
                        new_dir_entry = (void *) dir_entry + dir_entry->rec_len;
                        new_dir_entry->inode = allocate_free_inode(sb, gd);
                        new_dir_entry->rec_len = EXT2_BLOCK_SIZE - size -
                        dir_entry->rec_len;
                        new_dir_entry->name_len = strlen(last_path);
                        new_dir_entry->file_type = EXT2_FT_DIR;
                        memcpy(new_dir_entry->name, last_path,
                               new_dir_entry->name_len);
                    }
                }
            } else {
                
                fprintf(stderr, "No such file or directory\n");
                return ENOENT;
            }
        } else {
            
            if (pathname_tokens == NULL) {
                    
                fprintf(stderr, "File exists\n");
                return EEXIST;
            } else {
                
                if (dir_entry->file_type == 2) {
                    
                    dir_inum = dir_entry->inode;
                    dir_inode = (struct ext2_inode *)
                    (disk + (BLOCK_OFFSET(gd->bg_inode_table) + (dir_inum - 1)
                             * sizeof(struct ext2_inode)));
                } else {
                    
                    fprintf(stderr, "Not a directory\n");
                    return ENOENT;
                }
            }
        }
    }
    return 0;
}