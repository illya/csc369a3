#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "ext2.h"

unsigned char *disk;


int main(int argc, char **argv) {

    if(argc != 2) {
        fprintf(stderr, "Usage: readimg <image file name>\n");
        exit(1);
    }
    int fd = open(argv[1], O_RDWR);

    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
	perror("mmap");
	exit(1);
    }

    struct ext2_super_block * sb = (struct ext2_super_block *)(disk + 1024);
    printf("Inodes: %d\n", sb->s_inodes_count);
    printf("Blocks: %d\n", sb->s_blocks_count);

    struct ext2_group_desc * gd = (struct ext2_group_desc *)(disk +2048);
    printf("Block group:\n");
    printf("\tblock bitmap: %u\n", gd->bg_block_bitmap);
    printf("\tinode bitmap: %u\n", gd->bg_inode_bitmap);
    printf("\tinode table: %u\n", gd->bg_inode_table);
    printf("\tfree blocks: %hu\n", gd->bg_free_blocks_count);
    printf("\tfree inodes: %hu\n", gd->bg_free_inodes_count);
    printf("\tused_dirs: %hu\n", gd->bg_used_dirs_count);

    printf("Block bitmap: ");

    unsigned char mask = 1;
    for (int i = 0; i < 16; i ++){
        char * c = (char *)(disk + 3072 + i);
        unsigned char bits[8] ;
        for (int j = 0; j < 8; j++){
            bits[j] = ((*c & (mask << j)) != 0);
            printf("%d", bits[j]);
        }
        printf("  ");
       
    }

    printf("\nInode bitmap: ");

    for (int i = 0; i < 4; i ++){
        char * c = (char *)(disk + 4096 + i);
        unsigned char bits[8] ;
        for (int j = 0; j < 8; j++){
            bits[j] = ((*c & (mask << j)) != 0);
            printf("%d", bits[j]);
        }
        printf("  ");
       
    }
    printf("\n\nInodes:\n");
    char type = 'f';
    struct ext2_inode * inodes = (struct ext2_inode *)(disk + (5*1024));
    int num_dirs = gd->bg_used_dirs_count - 1 ;
    int dirs [num_dirs];
    int count = 0;
    
    for (int i = 1; i < 32; i++){
        
        if (inodes[i].i_size > 0){
            struct ext2_inode inode = inodes[i];
            if (inode.i_mode & EXT2_S_IFDIR){
                type = 'd';
                dirs[count] = i + 1;
                count++;
            } else if (inode.i_mode & EXT2_S_IFREG){
                type = 'f';
            } else if (inode.i_mode & EXT2_S_IFLNK){
                type = 'l';
            }

            printf ("[%d] type: %c size: %d links: %d blocks: %d\n", i+1, type, inode.i_size, inode.i_links_count, inode.i_blocks);
            printf ("[%d] Blocks: %u\n", i+1, inode.i_block[0]);

        }
        if(i == 1){
            i = 10;
        }

    }

    
    printf("\nDirectory Blocks:\n");

    for (int i = 0; i < num_dirs; i++){
        struct ext2_inode inode = inodes[dirs[i] - 1];
        int block = inode.i_block[0];
        printf("\tDIR BLOCK NUM: %d (for inode %d)\n", block, dirs[i]);
        int progress = 0;
        while(progress < 1024){
            struct ext2_dir_entry_2 * de = (struct ext2_dir_entry_2 *) (disk + (block*1024)+ progress);
            progress += (de->rec_len);
            char * name = de->name;
            name[de->name_len] = '\0';
            char c = 'u';
            if (de->file_type == 1){
                c = 'f';
            } else if (de->file_type == 2){
                c = 'd';
            } else if (de->file_type == 7){
                c = 'l';
            }
            printf("Inode: %d rec_len: %hu name_len: %d type= %c name=%s\n", de->inode, de->rec_len, de->name_len, c, name);
        }

    }

    return 0;
}
