#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ext2.h"

extern unsigned char* disk;

int step_forward(char * source, char * output) {
    // index in path to which we will step forward to afer the step
    // it is also the "step distance"
    int next_index;
    if (source[0] != '/') {
        return -1;
    }
    for (next_index = 1; source[next_index] != '/'; next_index++) {
        if (source[next_index] == '\0') {
            break;
        }
        output[next_index-1] = source[next_index];
    }
    output[next_index - 1] = '\0';
    return next_index;
}

int path_to_inode(char * path, void * inodes) {
    /* Step through the path, stepping forward each token
    separated by a slash, we start at the root inode (inode #2) */

    // Initialize inode and block counters, and the struct pointers
    // to a dir entry and inode
    int i = EXT2_ROOT_INO;
    int b;
    struct ext2_inode * inode;
    struct ext2_dir_entry_2 * dir_entry;
    char current_location[255] = " ";

    while (1) {

        inode = (struct ext2_inode * )
            (inodes + (i - 1) * sizeof(struct ext2_inode));
        // inode 2 is located in inodes[1] so inodes + (i - 1)*sizeof (inode)
        int total_bytes = inode -> i_size;
          // if path is / we know this is the root and inode is inode 2
          // found at inodes[1] so return i - 1
        if (strcmp(path, "/") == 0) {
            return i - 1;
        }
        if (strcmp(path, "") == 0) {
            path[0] = '/';
            path[1] = '\0';
        }
        // make the step forward
        int step_size = step_forward(path, current_location);
        // step forward in the path by step_size
        path += step_size;
        // we need to iterate through entire dir entry represented
        // by this inode
        int done_bytes = 0;
        int next_inode = -1;
        int i_block_index = 0;
        // block number inode pointing to 
        b = inode -> i_block[i_block_index];
        dir_entry = (struct ext2_dir_entry_2 * )
        (disk + EXT2_BLOCK_SIZE * b);
        while (done_bytes < total_bytes) {
            done_bytes += dir_entry -> rec_len;
            if (dir_entry -> file_type == EXT2_FT_DIR) {
                if (strlen(current_location) == dir_entry -> name_len &&
                    strncmp(current_location, dir_entry -> name,
                     dir_entry -> name_len) == 0) {
                    next_inode = dir_entry -> inode;
                    break;
                }
            }
            dir_entry = (void * ) dir_entry + dir_entry -> rec_len;
            if (done_bytes % EXT2_BLOCK_SIZE == 0) { 
                i_block_index++;
                b = inode -> i_block[i_block_index];
                dir_entry = (struct ext2_dir_entry_2 * )
                (disk + EXT2_BLOCK_SIZE * b);
            }
        }
        // if next_inode doesn't get altered above, return
        // otherwise go deeper
        if (next_inode == -1) {
            return -1;
        } else {
            i = next_inode;
        }
    }
}

// Traverse the block bitmap on the Heap and get the array of
// available blocks, if we have enough blocks available.
int * requesting_blocks(int * block_bm, int requested) {

    int i = 0;
    int j = 0;
    int * blocks = malloc(sizeof(int) * requested);
    while (i < requested) {
        while (block_bm[j] == 1) {
            j++;
            if (j == BLOCKS_COUNT) {
              // if we reach the end and there aren't enough blocks
              // return null, cannot fulfill request
                return NULL;
            }
        }
        blocks[i] = j;
        i++;
        j++;
    }
    return blocks;
}

int available_blocks(void * ptr) {
    int i, current_byte, current_bit;
    char * byte;
    int count = 0;
    for (i = 0; i < BLOCKS_COUNT; i++) {
        current_byte = i / 8;
        current_bit = i % 8;
        byte = ptr + current_byte;
        count += ( * byte >> current_bit) & 1;
    }
    return BLOCKS_COUNT - count;
}

int available_inodes(void * ptr) {
    int i, current_byte, current_bit;
    char * byte;
    int count = 0;
    for (i = 0; i < INODES_COUNT; i++) {
        current_byte = i / 8;
        current_bit = i % 8;
        byte = ptr + current_byte;
        count += ( * byte >> current_bit) & 1;
    }
    return INODES_COUNT - count;
}

void split_path(char * path, char * name) {
    int length = strlen(path);
    int i = length - 1;
    //go backwards from absolute path and extract name of last object
    while (path[i] != '/') {
        i--;
    }
    int name_len = length - i - 1;
    strncpy(name, path + length - name_len, name_len + 1);
    if (i == 0) { 
        path[i + 1] = '\0';
    } else {
        path[i] = '\0';
    }
}



// BITMAP GETTERS/SETTERS
// Bit arithmetic lines borrowed from StackOverlow
int * get_inode_bitmap(void * ptr) {

    int * inode_bitmap = malloc(sizeof(int) * INODES_COUNT);
    int i, current_byte, current_bit;
    for (i = 0; i < INODES_COUNT; i++) {
        current_byte = i / 8;
        current_bit = i % 8;
        char * byte = ptr + current_byte;
        inode_bitmap[i] = ( * byte >> current_bit) & 1;
    }
    return inode_bitmap;
}

int * get_block_bitmap(void * ptr) {
    int * block_bitmap = malloc(sizeof(int) * BLOCKS_COUNT);
    int i, current_byte, current_bit;
    for (i = 0; i < BLOCKS_COUNT; i++) {
        current_byte = i / 8;
        current_bit = i % 8;
        char * byte = ptr + current_byte;
        block_bitmap[i] = ( * byte >> current_bit) & 1;
    }
    return block_bitmap;
}

void set_inode_bitmap(void * ptr, int inode, int value) {
    int current_byte = inode / 8;
    int current_bit = inode % 8;
    char * byte = ptr + current_byte; *
    byte = ( * byte & ~(1 << current_bit)) | (value << current_bit);
}

void set_block_bitmap(void * ptr, int block, int value) {
    int current_byte = block / 8;
    int current_bit = block % 8;
    char * byte = ptr + current_byte; *
    byte = ( * byte & ~(1 << current_bit)) | (value << current_bit);
}

