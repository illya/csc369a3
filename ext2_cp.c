#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>
#include "ext2.h"
unsigned char * disk;

int main(int argc, char ** argv) {

    //check syntax
    if (argc != 4) {
        fprintf(stderr,
            "Usage: ext2_cp <img file> <SRC> <TARG>\n");
        exit(1);
    }

    //open image
    int fd = open(argv[1], O_RDWR);

    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
        perror("mmap");
        exit(1);
    }



    //open source file to copy
    int fdsrc = open(argv[2], O_RDONLY);



    //could not find source
    if (fdsrc == -1) {
        fprintf(stderr, "Could not find directory/file\n");
        return ENOENT;
    }

    //initialization
    char targ_file_name[256];
    int i = strlen(argv[2]) - 1;
    int new_inum = -1;
    int indirection = 0;
    int found = 0;
    int amount_copied = 0;

    //Determine how big the source is and how many blocks are needed
    int src_size = lseek(fdsrc, 0, SEEK_END);

    int src_blocks = (src_size - 1) / 1024 + 1;
    
    unsigned char * src_file = mmap(NULL, src_size, PROT_READ | PROT_WRITE,
        MAP_PRIVATE, fdsrc, 0);
    
    char * path_targ = (argv[3]);

    //Path must be absolute
    if (path_targ[0] !='/') {
        fprintf(stderr, "Could not find directory/file\n");
        return ENOENT;
    }

    //We accept backslashes at the end of the path but remove them for simplicity
    if ((path_targ[strlen(argv[3]) - 1] == '/')&& (strcmp(path_targ, "/")!=0)) {

        path_targ[strlen(argv[3]) - 1] = '\0';
    }

    //Get filename from path
    while ((argv[2][i] != '/') && (i >= 0)) {

        i--;
    }
    int filename_size = strlen(argv[2]) - i - 1;
    strncpy(targ_file_name, argv[2] + strlen(argv[2]) - filename_size, filename_size + 1);


    //Get bitmaps/tables needed for book keeping later on
    struct ext2_group_desc *gd = (struct ext2_group_desc *)(disk + 2048);
    void * inode_table = disk + 1024 * gd->bg_inode_table;
    int * inode_bitmap = get_inode_bitmap(disk + 1024 * gd->bg_inode_bitmap);
    int * block_bitmap = get_block_bitmap(disk + 1024 * gd->bg_block_bitmap);
    int parent_inum = path_to_inode(path_targ, inode_table);

    //if name given for file
    if (parent_inum ==-1) {

        split_path(path_targ, targ_file_name);

        filename_size = strlen(targ_file_name);

        //attempt to find directory before name
        parent_inum = path_to_inode(path_targ, inode_table);

        if (parent_inum < 0) {

            fprintf(stderr, "Could not find target directory/file\n");

            return ENOENT;
        }
    }
 
    int bnum, de_size;
    struct ext2_dir_entry_2 * dir;

    struct ext2_inode * parent_inode =
            inode_table + sizeof(struct ext2_inode) * parent_inum;
    int parent_blocks = parent_inode->i_size / 1024;

    //Looking for file that already has the same name
    for (i = 0; i<parent_blocks; i++) {

        bnum = parent_inode->i_block[i];

        dir = (void * ) disk + 1024 * bnum;

        de_size = 0;

        //Loop through directory entries
        while (de_size < 1024) {

            de_size += dir->rec_len;
            //If entry is a file and has same size, return EEXIST
            if (dir->file_type == EXT2_FT_REG_FILE &&
                filename_size == dir->name_len &&
                strncmp(targ_file_name, dir->name, filename_size) == 0) {

                fprintf(stderr, "File already exsists\n");
                return EEXIST;
            }

            dir = (void * ) dir + dir->rec_len;
        }
    }


    //Find a free inode
    for (i = 0; i < INODES_COUNT; i++) {

        //If bitmap[i] is 0, the inode at i is not in use
        if (inode_bitmap[i] == 0) {  

            new_inum = i;
            break;
        }
    }

    //No space error handling
    if (new_inum == -1) {

        fprintf(stderr, "No inodes left\n");
        return ENOSPC;
    }


    //Get an array of blocks we will be using
    int * blocks_to_use = requesting_blocks(block_bitmap, src_blocks);

    //No space error checking
    if (blocks_to_use == NULL) {

        fprintf(stderr, "No more blocks\n");
        return ENOSPC;
    }

    //Check if indirection is going to be needed
    if (src_blocks > 13) {

        src_blocks +=1; 
        indirection = 1;
    }


    int amount_to_copy = src_size;
    int blocks_to_write, current_block;

    //amount of blocks that need to be written
    if(indirection){
        blocks_to_write = src_blocks-1;
    }
    else{
        blocks_to_write = src_blocks;
    }
    
    //Loop through blocks while copying file
    for (i = 0; i < blocks_to_write; i++) {


        current_block = blocks_to_use[i];      
        set_block_bitmap(block_bitmap, current_block, 1);

       
        if (amount_to_copy > 1024) {
            //If more than a block size left, copy to block and go to next


            memcpy(disk + 1024 * (current_block + 1),src_file + 
                amount_copied, 1024);
            amount_to_copy -= 1024;
            amount_copied += 1024;
        } else {
            //If less than a block size left, copy to last block
            memcpy(disk + 1024 * (current_block + 1),src_file + amount_copied, 
                amount_to_copy);
            amount_to_copy = 0;
            break;
        }
    }

    current_block = -1;
    int * indirect_blocks;

    //If we need to handle indirection
    if (indirection) {
        //
        current_block = blocks_to_use[src_blocks - 1];

        set_block_bitmap(block_bitmap, current_block, 1);

        indirect_blocks = (void * ) disk + 1024 * (current_block + 1);

        for (i = 12; i < src_blocks - 1; i++) { 
            //Set the blocks in the single indirect table appropriately
            *indirect_blocks = blocks_to_use[i] + 1;

            indirect_blocks++;
        }
    }

    // new inode for copied file
    struct ext2_inode * new_inode = inode_table + 
        sizeof(struct ext2_inode) * new_inum;

    //Set appropriate meta data
    new_inode->i_size = src_size; 
    new_inode->i_mode = EXT2_S_IFREG;

    new_inode->i_blocks = src_blocks * 2;
    new_inode->i_links_count = 1;


    for (i = 0; i < src_blocks; i++) {
        if (i > 11) {
            break;
        }
        //Set each i_block element to its appropriate block
        new_inode->i_block[i] = blocks_to_use[i] + 1;
    }

    if (indirection) {
        //Handle indirection
        new_inode->i_block[12] = current_block + 1;
    }

    //Bitmap metadata
    set_inode_bitmap(inode_bitmap, new_inum, 1);

    
    int new_de_size = shift4(filename_size) + 8;
    
    //loop through blocks
    for (i = 0; i < parent_blocks; i++) {

        bnum = parent_inode->i_block[i];
        dir = (void * ) disk + 1024 * bnum;
        int de_size = 0;

        //loop through directory entries
        while (de_size < 1024) {

            de_size += dir->rec_len;
            if (de_size == 1024 &&
                dir->rec_len >= new_de_size + 8 + shift4(dir->name_len)) {
                //If there is room in this block, break
                found = 1;
                break;
            }
            dir = (void * ) dir + dir->rec_len;
        }
        if (found) {

            break;
        }
    }
    //If there is not room in this block
    if (!found) {

        block_bitmap = get_block_bitmap(block_bitmap);
        //Get a new block
        int * new_block = requesting_blocks(block_bitmap, 1);

        //No space error checking
        if (new_block == NULL) {

            fprintf(stderr, "No more blocks\n");
            return ENOSPC;
        }

        int next_block = * new_block;
        dir = (void * ) disk + 1024 * (next_block + 1);

        dir->file_type = EXT2_FT_REG_FILE;
        dir->rec_len = 1024;
        dir->name_len = filename_size;
        dir->inode = new_inum + 1;

        strncpy((void * ) dir + 8, targ_file_name, filename_size);

        //Bitmap and parent metadata
        set_block_bitmap(block_bitmap, next_block, 1);
        parent_inode->i_size += 1024;
        parent_inode->i_blocks += 2;
        parent_inode->i_block[parent_blocks] = next_block + 1;
    } 
    else {

        //Calculate sizes for later use
        int last_de_size = dir->rec_len;
        int new_size = shift4(dir->name_len) + 8;
        dir->rec_len = new_size;

        //Go to new de and 
        dir = (void * ) dir + new_size;

        //Fill out appropriate members
        dir->rec_len = last_de_size - new_size;
        dir->inode = new_inum + 1;
        dir->file_type = EXT2_FT_REG_FILE;      
        dir->name_len = filename_size;

        strncpy((void * ) dir + 8, targ_file_name, filename_size);
    }

    //Metadata
    gd->bg_free_inodes_count = available_inodes(inode_bitmap);

    gd->bg_free_blocks_count = available_blocks(block_bitmap);

    return 0;
}