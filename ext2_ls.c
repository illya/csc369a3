#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "ext2.h"

unsigned char *disk;

int main(int argc, char **argv) {
    
    if(argc != 3 && argc != 4) {
        fprintf(stderr, "Usage: ext2_ls <image file name>"
                "<absolute path> (-a optional flag)\n");
        exit(1);
    }
    int all = 0;
    if (argc == 4 ){
        if (strlen(argv[3]) == 2 && argv[3][0]=='-' && argv[3][1] == 'a'){
            all = 1;
        }
    }
    int fd = open(argv[1], O_RDWR);
    
    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
        perror("mmap");
        exit(1);
    }
    char * abs_path = argv[2];
    int path_len = strlen(abs_path);
    if (abs_path[0] != '/'){
        exit(ENOENT);
    } else if (path_len > 1 && abs_path[path_len - 1] != '/'){
        strncat(abs_path, "/", 1);
        path_len = strlen(abs_path);
    }
    int num_slashes = 0;
    int slash_loc [255];
    for (int i = 0; i < path_len; i++){
        if(abs_path[i] =='/'){
            slash_loc[num_slashes] = i;
            num_slashes++;
        }
    }
    //printf("Number of slashes: %d\n", num_slashes);
    struct ext2_group_desc *gd = (struct ext2_group_desc *)(disk +2048);
    int i_table = gd->bg_inode_table;
    struct ext2_inode * inodes = (struct ext2_inode *)(disk + (i_table*1024));
    struct ext2_inode current_node = inodes[1];
    
    
    int progress = 0;
    int found_dir = 0;
    int done = 0;
    int in_target_directory = 0;
    int block_index_in_inode = 0;
    int current_block = current_node.i_block[block_index_in_inode];
    
    // Loop to traverse the path and find the last directory in the absolute path
    // This loop can exit the program if the current directory is not found, since
    // downstream directories do not need to be traversed.
    while (progress <= num_slashes && !done){
        if (found_dir && (progress + 1) == num_slashes){
            in_target_directory = 1;
        }
        found_dir = 0;
        char current_dir[255];
        int cur_dir_name_length = slash_loc[progress+1] - slash_loc[progress] - 1;
        if(progress < (num_slashes - 1)){
            strncpy(current_dir, abs_path+slash_loc[progress] + 1,
                    cur_dir_name_length);
            current_dir[slash_loc[progress+1] - slash_loc[progress]] = '\0';
        }
        //printf("Current dir is: %s\n", current_dir);
        int sectors = current_node.i_blocks;
        while (sectors > 0 && !found_dir){
            int seek = 0;
            while(seek < 1024){
                struct ext2_dir_entry_2 * de =
                (struct ext2_dir_entry_2 *) (disk + (current_block*1024) + seek);
                seek += (de->rec_len);
                char * name = de->name;
                name[de->name_len] = '\0';
                //printf("Current dir entry is for: %s\n", name);
                // If absolute path == /
                if (num_slashes == 1){
                    in_target_directory = 1;
                    done = 1;
                    if (all){
                        printf("%s\n", name);
                    } else {
                        if (strcmp(name, ".") != 0 && strcmp(name, "..") != 0){
                            printf("%s\n", name);
                        }
                    }
                    
                }
                // If current dir-entry is the current dir from abs path
                // If this is a directory, update current node, sectors and
                // current block and keep traversing.
                //printf("Progress: %d \t Num Slashes: %d\n", progress, num_slashes);
                if ((strcmp(name, current_dir) == 0) &&
                    (progress < num_slashes)){
                    if (de->file_type == 2){
                        current_node = inodes[de->inode - 1];
                        sectors = current_node.i_blocks + 2;
                        //printf("going from block %d to %d\n", current_block, current_node.i_block[0]);
                        current_block = current_node.i_block[0];
                        found_dir = 1;
                        break;
                    } else {
                        if ((progress + 1) == num_slashes){
                            printf("%s", name);
                            exit(0);
                        }
                        printf("No such file or directory\n");
                        exit(ENOENT);
                    }
                }
                if ((progress == num_slashes) && (num_slashes != 1)){
                    if (all){
                        printf("%s\n", name);
                    } else if (strcmp(name, ".") != 0 && strcmp(name, "..") != 0){
                        printf("%s\n", name);
                    }
                    found_dir = 1;
                }
            }
            sectors = sectors - 2;
            block_index_in_inode++;
            if (sectors > 0 && block_index_in_inode == 12){
                unsigned int loc = (current_node.i_block[12])
                + sizeof(int)* (block_index_in_inode%12);
                current_block = (unsigned int)(disk + loc);
            }
        }
        progress++;
    }
    if (!in_target_directory){
        printf("No such file or directory\n");
        exit(ENOENT);
    }
    
    return 0;
}