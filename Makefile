default: ext2_ls ext2_ln ext2_mkdir ext2_rm ext2_cp 

ext2_ls: ext2_ls.o ext2.o
	gcc -std=c99 -Wall -Werror -g -o ext2_ls ext2_ls.o ext2.o

ext2_cp: ext2_cp.o ext2.o
	gcc -std=c99 -Wall -Werror -g -o ext2_cp ext2_cp.o ext2.o

ext2_mkdir: ext2_mkdir.o ext2.o
	gcc -std=c99  -Wall -Werror -g -o ext2_mkdir ext2_mkdir.o ext2.o

ext2_ln: ext2_ln.o ext2.o
	gcc -std=c99 -Wall -Werror -g -o ext2_ln ext2_ln.o ext2.o

ext2_rm: ext2_rm.o ext2.o
	gcc -std=c99 -Wall -Werror -g -o ext2_rm ext2_rm.o ext2.o

%.o: %.c
	gcc -Wall -g -c $<

clean:
	-rm -f ext2_ls ext2_ln ext2_mkdir ext2_rm ext2_cp readimage  *.o